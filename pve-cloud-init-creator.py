#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK
'''create cloud-init based virtual machines on proxmox ve'''


import argcomplete
import configargparse
import io
import yaml
import logging
import coloredlogs
import verboselogs
import time
from urllib.parse import quote
from minio import Minio
from proxmoxer import ProxmoxAPI, ResourceException
from _version import __version__


def get_logger(name):
    verboselogs.install()
    logger = logging.getLogger(name)
    coloredlogs.install(fmt='%(asctime)s %(hostname)s %(levelname)s %(message)s',
                        logger=logger)
    return logger


DEFAULT_USER_DATA = {'manage_etc_hosts': True,
                     'chpasswd': {
                         'expire': False
                     },
                     'users': [
                         'default'
                     ],
                     'package_upgrade': True}


def parse_arguments():
    '''argument parser'''

    parser = configargparse.ArgParser(prog='pve-cloud-init-creator')
    parser.add('vm')
    parser.add('--version',
               action='version',
               version=__version__)
    parser.add('--pve-host',
               env_var='PVE_HOST',
               required=True)
    parser.add('--pve-port',
               type=int,
               env_var='PVE_PORT',
               default=8006)
    parser.add('--pve-user',
               env_var='PVE_USER',
               required=True)
    parser.add('--pve-password',
               env_var='PVE_PASSWORD',
               required=True)
    parser.add('--pve-verify-ssl',
               env_var='PVE_VERIFY_SSL',
               action='store_true')
    parser.add('--pve-node',
               env_var='PVE_NODE',
               required=True)
    parser.add('--s3-endpoint',
               env_var='S3_ENDPOINT',
               required=True,
               help='s3 endpoint (host:port) for uploading snippets')
    parser.add('--s3-verify-ssl',
               env_var='S3_VERIFY_SSL',
               default=True,
               action='store_true')
    parser.add('--s3-access-key',
               env_var='S3_ACCESS_KEY',
               required=True,
               help='s3 access key for uploading snippets')
    parser.add('--s3-secret-key',
               env_var='S3_SECRET_KEY',
               required=True,
               help='s3 secret key for uploading snippets')
    parser.add('--s3-bucket',
               env_var='S3_BUCKET',
               required=True,
               help='s3 bucket for uploading snippets')
    subparsers = parser.add_subparsers(dest='action')
    subparsers.required = True
    create_parser = subparsers.add_parser('create')
    cloudinit_group = create_parser.add_mutually_exclusive_group(required=True)
    cloudinit_group.add('--vm-ssh-key',
                        env_var='VM_SSH_KEY',
                        nargs='?')
    cloudinit_group.add('--vm-cloudinit-userdata',
                        env_var='VM_CLOUDINIT_USERDATA',
                        nargs='?',
                        help='file containing cloud-init user config')
    create_parser.add('--pve-custom-cloud-init',
                      env_var='PVE_CUSTOM_CLOUDINIT',
                      action='store_true')
    create_parser.add('--pve-snippet-storage',
                      env_var='PVE_SNIPPET_STORAGE',
                      required=True)
    create_parser.add('--pve-pool',
                      env_var='PVE_POOL',
                      required=True)
    create_parser.add('--pve-template-id',
                      type=int,
                      env_var='PVE_TEMPLATE_ID',
                      required=True)
    create_parser.add('--vm-storage-size',
                      env_var='VM_STORAGE_SIZE',
                      default='8G')
    destroy_parser = subparsers.add_parser('destroy')
    argcomplete.autocomplete(parser)
    return parser.parse_args()


def minioclient():
    return Minio(args.s3_endpoint,
                 access_key=args.s3_access_key,
                 secret_key=args.s3_secret_key,
                 secure=args.s3_verify_ssl)


def create_snippet_from_scratch():
    minio = minioclient()
    ci = yaml.dump(DEFAULT_USER_DATA, default_flow_style=False)
    upload = io.BytesIO(f'#cloud-config\n{ci}'.encode())
    minio.put_object(args.s3_bucket,
                     f'{args.vm}.cfg',
                     upload,
                     upload.getbuffer().nbytes)


def create_snippet_from_file():
    logger.info('uploading cloudinit snippet')
    try:
        minio = minioclient()
        minio.fput_object(args.s3_bucket,
                          f'{args.vm}.cfg',
                          args.vm_cloudinit_userdata)
    except FileNotFoundError:
        logger.error(f'file {args.vm_cloudinit_userdata} not found')


def destroy_snippet():
    logger.info('deleting cloudinit snippet')
    try:
        if config.get('name'):
            minio = minioclient()
            minio.remove_object(args.s3_bucket,
                                f'{config.get("name")}.cfg')
    except Exception as e:
        logger.error(e)


def proxmoxclient():
    return ProxmoxAPI(args.pve_host,
                      port=args.pve_port,
                      user=args.pve_user,
                      password=args.pve_password,
                      verify_ssl=args.pve_verify_ssl)


def create_vm():
    proxmox = proxmoxclient()
    newid = proxmox.cluster.nextid.get()
    logger.info(f'VMID={newid}')
    node = proxmox.nodes(args.pve_node)
    logger.info('cloning vm')
    try:
        upid = node.qemu(args.pve_template_id).clone.post(newid=newid,
                                                          name=args.vm,
                                                          pool=args.pve_pool)
        time.sleep(5)
        while node.tasks(upid).status.get().get('status') == 'running':
            logger.info('still cloning')
            time.sleep(5)
        time.sleep(5)
        logger.info('resizing disk')
        node.qemu(newid).resize.put(disk='scsi0', size=args.vm_storage_size)
        time.sleep(5)
        logger.info('configuring vm')
        if args.vm_ssh_key:
            node.qemu(newid).config.post(
                sshkeys=quote(args.vm_ssh_key, safe=''),
                ipconfig0='ip=dhcp',
                ostype='l26')
        elif args.vm_cloudinit_userdata:
            node.qemu(newid).config.post(
                cicustom=f'user={args.pve_snippet_storage}:snippets/{args.vm}.cfg',
                ipconfig0='ip=dhcp',
                ostype='l26')
        time.sleep(5)
        logger.info('starting vm')
        node.qemu(newid).status.start.post()
    except ResourceException as e:
        logger.error(e)


def destroy_vm():
    proxmox = proxmoxclient()
    node = proxmox.nodes(args.pve_node)
    logger.info('shutting down vm')
    try:
        node.qemu(args.vm).status.shutdown.post()
        while node.qemu(args.vm).status.current.get().get('status') == 'running':
            logger.info('vm still running')
            time.sleep(5)
        logger.info('deleting vm')
        node.qemu(args.vm).delete()
    except ResourceException as e:
        logger.error(e)


def get_vm_config():
    proxmox = proxmoxclient()
    node = proxmox.nodes(args.pve_node)
    logger.info('fetching vm config')
    try:
        config = node.qemu(args.vm).config.get()
        return config
    except ResourceException as e:
        logger.error(e)


if __name__ == "__main__":
    logger = get_logger('pve-cloud-init-creator')
    args = parse_arguments()
    if args.action == 'create':
        if args.vm_ssh_key:
            create_snippet_from_scratch()
        elif args.vm_cloudinit_userdata:
            create_snippet_from_file()
        DEFAULT_USER_DATA.update({'hostname': args.vm,
                                  'ssh_authorized_keys': [args.vm_ssh_key]})
        create_vm()
    elif args.action == 'destroy':
        config = get_vm_config()
        destroy_vm()
        destroy_snippet()
