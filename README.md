# Proxmox VE Cloud-Init Creator

## Scope

Proxmox VE supports [Cloud Init](https://pve.proxmox.com/wiki/Cloud-Init_Support) so one can easily bootstrap VM's without PXE/Kickstart/Preseed/...

This project provides a single python program ([pre-compiled](https://pyinstaller.readthedocs.io/) and [statically linked](https://github.com/JonathonReinhart/staticx/)) to do so using the following tools:

* [proxmoxer](https://github.com/proxmoxer/proxmoxer/): PVE API communication
* [Minio](https://min.io/): S3 object storage server for snippets (until snippet upload is possible through API itself)
* [Packer (optional)](https://www.packer.io/): If you want to customize the cloud images

Motivation was to setup dynamic Gitlab runners for a multiarch docker image build [here](https://gitlab.com/morph027/signal-web-gateway/pipelines/134306488).

## Installation

Download latest binary from [here](https://gitlab.com/morph027/pve-cloud-init-creator/-/jobs/artifacts/master/raw/pve-cloud-init-creator?job=binary) or use the Docker image `registry.gitlab.com/morph027/pve-cloud-init-creator:latest`.

## Preparation

### Snippet storage

Unfortunately, PVE API does not (yet) allow to upload snippets so we're using a little workaround.

* create a directory and add to PVE storage (tick snippets)
* spin up Minio S3 server using this directory

#### Example

* `zfs create rpool/minio`
* download Minio:
  * `curl -Lo /usr/local/bin/minio https://dl.min.io/server/minio/release/linux-amd64/minio`
  * `chmod +x /usr/local/bin/minio`
* start Minio:
  * `minio server /rpool/minio`
* download Minio Client:
  * `curl -Lo /usr/local/bin/minioclient https://dl.min.io/client/mc/release/linux-amd64/mc`
  * `chmod +x /usr/local/bin/minioclient`
* create bucket (use keys from server start command):
  * `minioclient config host add pve http://localhost:9000 <access-key> <secret-key>`
  * `minioclient mb pve/snippets`
* add storage to PVE cluster:
  * Type: Directory
  * Directory: `/rpool/minio/snippets`
  * Content: Snippets

### PVE

* create a template vm like described [here](https://pve.proxmox.com/wiki/Cloud-Init_Support) (recommended image: [Ubuntu Minimal](https://cloud-images.ubuntu.com/minimal/daily/focal/current/focal-minimal-cloudimg-amd64.img))
* create a dedicated pool
* create a dedicated user
* create a dedicated storage for vms
* add the template vm to the pool
* add user permissions:
  * PVEVMAdmin for the pool
  * PVEDatastoreAdmin for the vm storage
  * PVEDatastoreUser for the snippet storage created in previous step

## Usage

`pve-cloud-init-creator --help` gives you an overview about all possible and required options. You're able to set environment variables instead of providing the arguments.

### Example with default cloud init data

```
pve-cloud-init-creator \
--pve-host proxmox.example.com \
--pve-port 443 \
--pve-user user@pve \
--pve-node node \
--s3-endpoint minio.example.com \
--s3-access-key access \
--s3-secret-key secret \
my-dummy-vm \
create \
--pve-snippet-storage snippets \
--pve-pool cloudinit \
--pve-template-id 9000 \
--vm-ssh-key "$(cat ~/.ssh/id_rsa.pub)"
```

This command will print a VMID which you can use to destroy the VM later:

```
export PVE_HOST=proxmox.example.com PVE_PORT=443 PVE_USER=user@pve PVE_NODE=node S3_ENDPOINT=minio.example.com S3_BUCKET=snippets S3_ACCESS_KEY=access S3_SECRET_KEY=secret
pve-cloud-init-creator \
<VMID> \
destroy
```

### Example with custom cloud init data

#### Cloud Init data

```yaml
#cloud-config
hostname: foobar
manage_etc_hosts: true
ssh_authorized_keys:
  - ssh-rsa ...
chpasswd:
  expire: False
users:
  - default
package_update: true
packages:
    - docker.io
write_files:
    - content: |
        #!/bin/bash
        
        echo "I'm a script to do something fancy on first boot"
      path: /var/lib/cloud/scripts/per-once/01-fancy-script.sh
      permissions: '0755'
```

#### Create

Instead of `--vm-ssh-key`, use `--vm-cloudinit-userdata <file>`.

## Extra

### Custom Images

If you need to modify the cloud-init images, you can use packer described [here](https://discourse.ubuntu.com/t/building-multipass-images-with-packer/12361)
